FROM python:3.6-slim

COPY . /app

WORKDIR /app

RUN apt-get update \
    && pip install --upgrade pip \
    && pip install --upgrade setuptools \
    && pip install -r requirements.txt \
    && mkdir -p log

CMD [ "python", "app.py" ]