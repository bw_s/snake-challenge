board_help = """
The board is an array describing the dimensions of the board.board[0] stands for the
number of rows, and board[1] corresponds to the number of columns.
Guaranteed constraints:
    board.length = 2,
    1 ≤ board[i] ≤ 10.
Example -b "[5,3]"
"""

snake_help = """
The snake is an array that describes the configuration of the snake's body on the
board. snake[0] corresponds to the initial coordinates of the snake's head, snake[1]
corresponds to coordinates of the second cell, etc.
It is guaranteed that snake[i] and snake[i + 1] are horizontally or vertically adjacent,
and that its initial configuration is valid (i.e. there are no self-intersections and the
snake's entire body lies inside the board).
Guaranteed constraints:
    3 ≤ snake.length ≤ 7,
    snake[i].length = 2,
    0 ≤ snake[i][j] < board[j].
Example -s "[[0,2], [0,1], [0,0], [1,0], [1,1], [1,2]]"
"""

depth_help = """
The depth is the paths depth. You have to discard all paths with a different path
length.
Guaranteed constraints:
    1 ≤ n ≤ 20.
Example -d "10"
"""

# Board limitations
board_len = 2
min_board_size = 1
max_board_size = 10

# Snake limitations
snake_len_pos = 2
min_snake_pos_coor = 0
min_snake_size = 3
max_snake_size = 7

# Snake limitations
min_depth_size = 1
max_depth_size = 20
