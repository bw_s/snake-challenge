import constant


class SnakeBoard(object):
    def __init__(self, board):
        self.board = board

    def move(self, snake, movement):
        """
        Check if the movement is correct and return the coordinates of the new snake

        :param snake: ori snake
        :param movement: possible movement [U, L, D, R]
        :return: nex snake
        """
        next_mov = None
        if movement == 'U':
            if snake[0][1] > constant.min_snake_pos_coor:
                next_mov = [snake[0][0], snake[0][1] - 1]
        elif movement == 'L':
            if snake[0][0] > constant.min_snake_pos_coor:
                next_mov = [snake[0][0] - 1, snake[0][1]]
        elif movement == 'D':
            if snake[0][1] < self.board[1] - 1:
                next_mov = [snake[0][0], snake[0][1] + 1]
        elif movement == 'R':
            if snake[0][0] < self.board[0] - 1:
                next_mov = [snake[0][0] + 1, snake[0][1]]
        else:
            raise ValueError("Incorrect movement: {}".format(movement))

        if next_mov is not None and next_mov not in snake[:-1]:
            new_snake = [next_mov] + snake[:-1]
        else:
            return None

        return new_snake

    def max_depths(self, snake, depth):
        """
        Function to search the max movement
        :param snake: ori snake
        :param depth: movements
        :return: number of correct depths
        """
        return self._iter_depths(snake, depth, 1)

    def _iter_depths(self, snake, depth, iteration):
        count_correct = 0
        for mov in ['U', 'L', 'D', 'R']:
            new_snake = self.move(snake, mov)
            if new_snake is not None:
                if iteration < depth:
                    new_iter = iteration + 1
                    count_correct = count_correct + self._iter_depths(new_snake, depth, new_iter)
                else:
                    count_correct = count_correct + 1
        return count_correct
