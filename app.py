import logging

from arguments import Arguments
from snake_board import SnakeBoard


def count_depth(board, snake, depth):
    sb = SnakeBoard(board)
    result = sb.max_depths(snake, depth)
    return result


if __name__ == '__main__':
    logging.info("Running he main program.")
    var = Arguments()
    try:
        var.parse_arguments()
    except (SyntaxError, ValueError, TypeError) as e:
        logging.error(e)
        exit()
    print(count_depth(var.board, var.snake, var.depth))

