import unittest

from app import count_depth


class FirstsTests(unittest.TestCase):

    @staticmethod
    def test_example_1():
        board = [4, 3]
        snake = [[2, 2], [3, 2], [3, 1], [3, 0], [2, 0], [1, 0], [0, 0]]
        depth = 3
        result = count_depth(board, snake, depth)

        assert result == 7

    @staticmethod
    def test_example_2():
        board = [2, 3]
        snake = [[0, 2], [0, 1], [0, 0], [1, 0], [1, 1], [1, 2]]
        depth = 10
        result = count_depth(board, snake, depth)

        assert result == 1

    @staticmethod
    def test_example_3():
        board = [10, 10]
        snake = [[5, 5], [5, 4], [4, 4], [4, 5]]
        depth = 4
        result = count_depth(board, snake, depth)

        assert result == 81

    @staticmethod
    def test_example_4():
        board = [3, 3]
        snake = [[0, 0], [1, 0], [2, 0], [2, 1], [2, 2], [1, 2], [0, 2], [0, 1], [1, 1]]
        depth = 4
        result = count_depth(board, snake, depth)

        assert result == 0

    @staticmethod
    def test_example_5():
        board = [3, 3]
        snake = [[0, 0], [1, 0], [2, 0], [2, 1], [2, 2], [1, 2], [0, 2], [0, 1]]
        depth = 4
        result = count_depth(board, snake, depth)

        assert result == 4


if __name__ == '__main__':
    unittest.main()
