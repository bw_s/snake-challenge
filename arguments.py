from argparse import ArgumentParser, RawTextHelpFormatter
import ast

import constant


class Arguments(object):

    def __init__(self):
        self.board = None
        self.snake = None
        self.depth = None

    @staticmethod
    def _check_board(b):
        try:
            # Check if is a list
            board = ast.literal_eval(b)
        except (SyntaxError, ValueError):
            raise ValueError("Board format error: {}".format(b))

        # Check if board contains integers
        if not isinstance(board[0], int) or not isinstance(board[1], int):
            raise ValueError("Board values must be integer {}".format(board))

        # Check if board len is 2
        if len(board) != constant.board_len:
            raise ValueError("Board size must be 2: {}".format(board))

        # Check if board is between 1 and 10
        if board[0] < constant.min_board_size or board[1] < constant.min_board_size:
            raise ValueError("Board row or column size must be more than 0: {}".format(board))
        if board[0] > constant.max_board_size or board[1] > constant.max_board_size:
            raise ValueError("Board row or column size must be 10 or less: {}".format(board))

        return board

    def _check_snake(self, s):
        try:
            # Check if is a list
            snake = ast.literal_eval(s)
        except (SyntaxError, ValueError):
            raise ValueError("Snake format error: {}".format(s))

        # Check snake size
        if constant.min_snake_size > len(snake) > constant.max_snake_size:
            raise ValueError("Snake len must be between {} and {}: {}".format(constant.min_snake_size,
                                                                              constant.max_snake_size,
                                                                              snake))

        for idx, elem_list in enumerate(snake):
            # Check if each element is a list
            if not isinstance(elem_list, list):
                raise ValueError("This snake position element is not a list {} in snake {}".format(elem_list, snake))
            # Check position len
            if len(elem_list) != constant.snake_len_pos:
                raise ValueError("This snake positions len must be 2: {} in snake {}".format(elem_list, snake))

            # Check if snake position coordinates is integer
            if not isinstance(elem_list[0], int) or not isinstance(elem_list[1], int):
                raise ValueError("This snake position must be integer {} in snake {}".format(elem_list, snake))

            # Check if snake positions coordinates is more than min
            if elem_list[0] < constant.min_snake_pos_coor or elem_list[1] < constant.min_snake_pos_coor:
                raise ValueError("This snake position must be more than 0: {} in snake {}".format(elem_list, snake))

            # Check if snake positions coordinates is more than the board
            if elem_list[0] >= self.board[0] or elem_list[1] >= self.board[1]:
                raise ValueError("This snake position {} must be less "
                                 "than board position {} in snake {}".format(elem_list, self.board, snake))

            # Check if the snake positions are repeated
            if snake.count(elem_list) != 1:
                raise ValueError("This snake position {} is repeated in the snake {}".format(elem_list, snake))

            # Check if the snake is continuous
            if idx != (len(snake) - 1):
                if ((elem_list[0] + elem_list[1]) + 1 == (snake[idx + 1][0] + snake[idx + 1][1]) or (
                        elem_list[0] + elem_list[1]) - 1 == (snake[idx + 1][0] + snake[idx + 1][1])) and (
                        elem_list[0] == snake[idx + 1][0] or elem_list[1] == snake[idx + 1][1]):
                    continue
                else:
                    raise ValueError("The snake is not continuous. "
                                     "Position {} doesnt correlate with then next position {}".format(elem_list,
                                                                                                      snake[idx + 1]))
        return snake

    @staticmethod
    def _check_depth(d):

        try:
            # Check if is a list
            depth = int(d)
        except (SyntaxError, ValueError):
            raise ValueError("Depth value in not integer {}".format(d))

        if constant.min_depth_size >= depth >= constant.max_depth_size:
            raise ValueError("Depth value must be between {} and {}. Introduced: {}".format(constant.min_depth_size,
                                                                                            constant.max_depth_size,
                                                                                            depth))

        return depth

    def parse_arguments(self):
        parser = ArgumentParser(description="Arguments get parsed via --commands", formatter_class=RawTextHelpFormatter)

        parser.add_argument('-b', metavar='board', type=str, help=constant.board_help, required=True)
        parser.add_argument('-s', metavar='snake', type=str, help=constant.snake_help, required=True)
        parser.add_argument('-d', metavar='depth​', type=str, help=constant.depth_help, required=True)
        args = parser.parse_args()

        self.board = self._check_board(args.b)
        self.snake = self._check_snake(args.s)
        self.depth = self._check_depth(args.d)
